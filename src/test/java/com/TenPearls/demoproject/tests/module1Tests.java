package com.TenPearls.demoproject.tests;

import com.TenPearls.demoproject.PageClasses.module1Page;
import com.utils.BrowserConfig.WebConnector;
import org.testng.annotations.Test;

public class module1Tests extends WebConnector {

    module1Page mp1 = new module1Page();

    @Test
    public void accessChapter1(){
        mp1.OpenBaseURL();
        mp1.assertTextOnHome();
        mp1.clickChapter1();
        mp1.assertTextOnChapter1();
        mp1.clickHomePage();
        mp1.assertTextOnHome();
    }
}
