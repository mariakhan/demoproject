package com.TenPearls.demoproject.PageClasses;

import com.utils.BrowserConfig.GetWebDriver;
import com.utils.ConfigUtil;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import java.util.Properties;

public class module1Page extends GetWebDriver {
    static Properties config, moduleSelector;
    WebDriverWait wait;

    public module1Page(){
        config = ConfigUtil.getConfig("config");
        moduleSelector = ConfigUtil.getConfig("module1Selector");

    }

    public void OpenBaseURL(){
        String url = config.getProperty("URL");
        driver.get(url);
    }

    public void clickChapter1(){
        String locator = moduleSelector.getProperty("xpathChapter1");
        driver.findElement(By.xpath(locator)).click();
    }

    public void assertTextOnChapter1(){
        wait = new WebDriverWait(driver, 20);
        String locator = moduleSelector.getProperty("idAssertTextChapter1");
        WebElement element = wait.until(
                ExpectedConditions.visibilityOfElementLocated(By.id(locator)));
        Assert.assertEquals(element.getText(),"Assert that this text is on the page");
    }

    public void clickHomePage(){
        String locator = moduleSelector.getProperty("xpathHomePage");
        driver.findElement(By.xpath(locator)).click();
    }

    public void assertTextOnHome(){
        wait = new WebDriverWait(driver, 20);
        String locator = moduleSelector.getProperty("classHomeBody");
        WebElement element = wait.until(
                ExpectedConditions.visibilityOfElementLocated(By.className(locator)));
        Assert.assertTrue(element.getText().contains("Below is a list of links to the examples needed in the chapters"));
    }
}
